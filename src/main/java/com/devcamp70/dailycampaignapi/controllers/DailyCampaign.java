package com.devcamp70.dailycampaignapi.controllers;

import java.time.DayOfWeek;
import java.time.LocalDate;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class DailyCampaign {
   @CrossOrigin
   @GetMapping("campaigns")
   public String getDalyCampaign() {
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();
        String campaign = "";

        if(dayOfWeek == DayOfWeek.MONDAY){
            campaign = "Mua 1 tặng 1";
        }else if(dayOfWeek == DayOfWeek.SATURDAY){
            campaign = "Tặng tất cả khách hàng một phần bánh ngọt";
        }else{
            campaign = "Không có khuyến mại trong ngày hôm nay";
        }
        return campaign;
   }
}
