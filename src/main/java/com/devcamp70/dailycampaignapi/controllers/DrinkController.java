package com.devcamp70.dailycampaignapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp70.dailycampaignapi.models.Drink;
import com.devcamp70.dailycampaignapi.services.DrinkService;
import java.util.*;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class DrinkController {
    @Autowired
    DrinkService drinkService;

    @CrossOrigin
    @GetMapping("/drinks")

    public ArrayList<Drink> getAllDrink() {
        return drinkService.getArrayListDrink();
    }
}
