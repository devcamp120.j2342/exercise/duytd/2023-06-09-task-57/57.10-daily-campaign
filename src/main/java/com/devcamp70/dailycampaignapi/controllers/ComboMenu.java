package com.devcamp70.dailycampaignapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp70.dailycampaignapi.models.Menu;
import com.devcamp70.dailycampaignapi.services.MenuService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ComboMenu {
    @Autowired
    MenuService menuService;

    @CrossOrigin
    @GetMapping("/combomenu")
    public ArrayList<Menu> getAllMenus() {
        return  menuService.getAllMenusService();
    }    
}
